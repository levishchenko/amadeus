package com.amadeus.pages.product_list;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.HashMap;
import java.util.List;


public class ProductListPage extends PageObject{

    private int pageCount = 3;

    @FindBy(xpath = "//*[@class='g-i-tile-i-box'][descendant::i[@class='g-tag  g-tag-icon-middle-popularity sprite']]//*[@class='g-price-uah']")
    private List<WebElement> pricesOfTopSalesList;

    @FindBy(xpath = "//*[@class='g-i-tile-i-box'][descendant::i[@class='g-tag  g-tag-icon-middle-popularity sprite']]//div[@class='g-i-tile-i-title clearfix']")
    private List<WebElement> namesOfTopSalesList;

    @FindBy(xpath = "//*[@class='g-i-tile-i-box'][descendant::i[@class='g-tag  g-tag-icon-middle-popularity sprite']]")
    private List<WebElement> elementList;

    @FindBy(css = ".paginator-catalog-l-i.active+li")
    private WebElement nextPageButton;

    public HashMap<String, String> test(){
        HashMap<String, String> map = new HashMap<>();
        for (int i = 0; i < pageCount; i++){
            for (WebElement element : elementList){
                String price =  element.findElement(By.xpath(".//div[@class='g-price-uah']")).getText().replaceAll(" грн", "").replaceAll(" ", "");
                String headline = element.findElement(By.xpath(".//div[@class='g-i-tile-i-title clearfix']")).getText();
                map.put(headline, price);
            }
            nextPageButton.click();
        }
        return map;
    }
}
