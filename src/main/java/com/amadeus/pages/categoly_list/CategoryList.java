package com.amadeus.pages.categoly_list;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.WebElement;

public class CategoryList extends PageObject{

    @FindBy(xpath = "//a[contains(text(),'Телефоны, ТВ и электроника')]")
    private WebElement phoneAndElectronicsTab;

    @FindBy(xpath = "//ul[@id=\"menu_categories_left\"]//*[contains(text(),'Телефоны')]")
    private WebElementFacade phoneTab;

    @FindBy(xpath = "//ul[@id=\"menu_categories_left\"]//*[contains(text(),'Смартфоны')]")
    private WebElementFacade smartphoneTab;

    public void goToSmartphoneTab() {
        getDriver().get(phoneAndElectronicsTab.getAttribute("href"));
        phoneTab.click();
        smartphoneTab.click();
    }
}
