package com.amadeus.core;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class DataSql {

    protected DB db = DB.getInstance();

    public void insertProductNameAndPrice(HashMap<String, String> map) throws SQLException {
        for (Map.Entry<String, String> entry : map.entrySet()) {
            String sqlInsert = "Insert `product_top_sales` (`product_price`, `product_name`) VALUES (\"" + entry.getValue() + "\" , \"" + entry.getKey() + "\" )";
            db.insert(sqlInsert, db.dbAmadeus);
        }
    }
}
