package com.amadeus.core;

import org.apache.commons.dbcp.BasicDataSource;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class DB {
    private static DB instance;

    private static Config config = Config.getInstance();

    public Connection dbAmadeus = null;

    protected DB() {
        dbAmadeus = connectToAmadeus();
    }


    public static synchronized DB getInstance() {
        if (instance == null) {
            instance = new DB();
        }

        return instance;
    }


    private static Connection connectToAmadeus() {
        return connectToDb(config.get("amadeusDB"));
    }


    private static Connection connectToDb(String dbName) {

        Connection connection = null;
        BasicDataSource dataSource = new BasicDataSource();

        dataSource.setUrl("jdbc:mysql://" + config.get("db_host") + "/" + dbName + "?" + config.get("db_params"));
        dataSource.setDriverClassName(config.get("db_driver"));
        dataSource.setUsername(config.get("db_user"));
        dataSource.setPassword(config.get("db_password"));

        try {
            connection = dataSource.getConnection();
        } catch (SQLException e) {
            //...
        }

        return connection;
    }


    public ResultSet select(String sql, Connection connection) {
        ResultSet resultSet = null;
        try {
            Statement statement = connection.createStatement();
            resultSet = statement.executeQuery(sql);
        } catch(Exception e) {
            e.printStackTrace();
        }

        return resultSet;
    }


    public void insert(String sql, Connection connection) {
        try {

            Statement statement = connection.createStatement();
            statement.executeUpdate(sql);
            statement.close();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }


    public void delete(String sql, Connection conn) {
        try {
            Statement statement = conn.createStatement();
            statement.executeUpdate(sql);
            statement.close();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
}
