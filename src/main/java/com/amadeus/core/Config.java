package com.amadeus.core;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Config {

    private static Config instance;

    private Properties config;

    public Config(String filename) {

        try {
            config = new Properties();
            InputStream in = new FileInputStream(System.getProperty("user.dir") + "/" + filename);

            config.load(in);
            in.close();
        } catch (IOException exc) {
            Logger.getLogger(Config.class.getName()).log(Level.SEVERE, null, exc);
        }

    }

    public static synchronized Config getInstance() {
        if (instance == null) {
            instance = new Config("config.properties");
        }

        return instance;
    }

    public String get(String paramName) {
        return config.getProperty(paramName);
    }
}
