package com.amadeus.steps.main_page;

import com.amadeus.pages.main_page.MainPage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;


public class MainPageSteps extends ScenarioSteps {

    private MainPage mainPage;

    @Step
    public void openMainPage(){
        mainPage.open();
    }
}
