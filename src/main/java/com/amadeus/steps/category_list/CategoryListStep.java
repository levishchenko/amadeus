package com.amadeus.steps.category_list;

import com.amadeus.pages.categoly_list.CategoryList;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;


public class CategoryListStep extends ScenarioSteps{

    private CategoryList categoryList;

    @Step
    public void goToSmartphoneTab(){
        categoryList.goToSmartphoneTab();
    }
}
