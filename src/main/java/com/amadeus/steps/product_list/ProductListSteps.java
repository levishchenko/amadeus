package com.amadeus.steps.product_list;

import com.amadeus.pages.product_list.ProductListPage;
import net.thucydides.core.annotations.Step;
import java.util.HashMap;


public class ProductListSteps {

    private ProductListPage productListPage;

    @Step
    public HashMap<String, String> testElement(){
        return productListPage.test();
    }

}
