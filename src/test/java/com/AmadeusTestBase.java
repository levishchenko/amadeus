package com;

import com.amadeus.core.DataSql;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.ManagedPages;
import net.thucydides.core.pages.Pages;
import org.openqa.selenium.WebDriver;

public class AmadeusTestBase {

    public DataSql dataSql = new DataSql();

    @Managed
    public WebDriver driver;

    @ManagedPages(defaultUrl = "http://rozetka.com.ua/")
    public Pages pages;
}
