package com;


import com.amadeus.pages.product_list.ProductListPage;
import com.amadeus.steps.category_list.CategoryListStep;
import com.amadeus.steps.main_page.MainPageSteps;
import com.amadeus.steps.product_list.ProductListSteps;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Steps;
import net.thucydides.junit.annotations.Concurrent;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.sql.SQLException;

@RunWith(SerenityRunner.class)
@Concurrent(threads="1")
public class InsertTopProductToDbTest extends AmadeusTestBase {

    ProductListPage productListPage;

    @Steps
    public MainPageSteps mainPageSteps;

    @Steps
    public CategoryListStep categoryListStep;

    @Steps
    public ProductListSteps productListSteps;

    @Test
    public void insertTopProductToDB() throws SQLException {
        mainPageSteps.openMainPage();
        categoryListStep.goToSmartphoneTab();
        dataSql.insertProductNameAndPrice(productListSteps.testElement());
    }
}
