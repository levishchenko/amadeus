Для запуска теста необходимо:
------------
``` bash
1. Установить maven
2. Установить jdk версии 8
3. Сделать clone проекта https://bitbucket.org/levishchenko/amadeus
4. Загрузить дамп базы данных dumpForAmadeus.sql
5. В файле config.properties установить параметры доступа к базе данных
6. В домашней директории проекта выполнить mvn test
7. Для генерации отчета запустить mvn serenity:aggregate
Отчет можно посмотреть в директории  $path_to_project/amadeus/target/site/serenity/index.html
```

Тесты запускаются на браузере firefox.
В версии firefox 48 перестал работать FirefoxDriver, а новый драйвер GeckoDriver пока что еще не отличается стабильностью.
Поэтому нужно иметь более старую версию браузера firefox.
Браузер можно скачать по ссылке https://ftp.mozilla.org/pub/firefox/releases/44.0/